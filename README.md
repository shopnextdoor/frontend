# ShopNextDoor

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.7.

NodeJS/npm muss installiert sein.

## Nach Checkout

`npm i`

`npm i -g @angular/cli`


## Development server

`ng serve` für dev Server. Dann `http://localhost:4200/` aufrufen.

## Module, wo ist was?

`api.service.ts` beinhaltet die Aufrufe der API-Calls

`map.component.ts` ist die Google Map ansicht. Hier kann priorisiert weiter gemacht werden

Die weiteren Komponenten befinden sich in den jeweiligen Ordnern (header, landing, etc.)



## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
