import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HeaderComponent } from '../header/header.component';
import { ApiService } from '../api.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  displayId: string;
  lat = 51.081191;
  lng = 9.829326;
  zoom = 6;
  entries: any[] = [
    {
      "id": 1,
      "image": "https://source.unsplash.com/random/50x50",
      "companyName": "Beispiel Firma",
      "ownerName": "Max Mustermann",
      "zip": "030",
      "longitude": 10.686559,
      "latitude": 53.865467,
      "city": "Lübeck",
      "street": "Hüxstraße 1",
      "contactTimes": "Mo - Sa 08:00 - 18:00",
      "phone": "01234/567890",
      "email": "meinunternehmen@web.de",
      "facebook": "",
      "web": "meinunternehmen.de",
      "description": "Lorem ipsum...",
    },
    {
      "id": 2,
      "image": "https://source.unsplash.com/random/50x50",
      "companyName": "Beispiel Firma",
      "ownerName": "Max Mustermann",
      "zip": "030",
      "longitude": 13.404954,
      "latitude": 52.520008,
      "city": "Berlin",
      "street": "Karl-Marx-Straße 30",
      "contactTimes": "Mo - Sa 08:00 - 18:00",
      "phone": "",
      "email": "meinunternehmen@web.de",
      "facebook": "",
      "web": "",
      "description": "Lorem ipsum...",
    },
    {
      "id": 3,
      "image": "https://source.unsplash.com/random/50x50",
      "companyName": "Beispiel Firma",
      "ownerName": "Max Mustermann",
      "zip": "030",
      "longitude": 13.454954,
      "latitude": 52.540008,
      "city": "Berlin",
      "street": "Karl-Marx-Straße 30",
      "contactTimes": "Mo - Sa 08:00 - 18:00",
      "phone": "",
      "email": "meinunternehmen@web.de",
      "facebook": "",
      "web": "",
      "description": "Lorem ipsum...",
    },
    {
      "id": 4,
      "image": "https://source.unsplash.com/random/50x50",
      "companyName": "Beispiel Firma",
      "ownerName": "Max Mustermann",
      "zip": "030",
      "longitude": 7.1745,
      "latitude": 51.26147,
      "city": "Berlin",
      "street": "Karl-Marx-Straße 30",
      "contactTimes": "Mo - Sa 08:00 - 18:00",
      "phone": "",
      "email": "meinunternehmen@web.de",
      "facebook": "",
      "web": "",
      "description": "Lorem ipsum...",
    }
  ];
  constructor(private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private api : ApiService) {
    HeaderComponent.instance.title = 'Karte';
    this.api.getAllEntries().subscribe((e) => {
      this.entries = e;
    }, (error) => {
      this.snackBar.open('Fehler bei der Anfrage im Backend');
    })
    this.route.params.subscribe((params) => {
      console.log(params);
      this.displayId = params.id;
    });
    navigator.geolocation.getCurrentPosition( pos => {
      this.lng = +pos.coords.longitude;
      this.lat = +pos.coords.latitude;
      this.zoom = 10;
    });
  }
  get(id: string|number){
    return this.entries.find((e) => e.id == id);
  }
  open(entry: any){
    this.displayId = entry ? entry.id : null;
    /*
    if(entry){
      this.router.navigate(['customer','entry',entry.id]);
    } else {
      this.router.navigate(['customer']);
    }
    */
  }
  ngOnInit(): void {
  }

}
