import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatStepperModule} from '@angular/material/stepper';
import {AgmCoreModule} from '@agm/core';
import { HeaderComponent } from './header/header.component';
import { MerchantRegisterComponent } from './merchant-register/merchant-register.component';
import { InputComponent } from './input/input.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { ImagePickerComponent } from './image-picker/image-picker.component';
import { CookieBannerComponent } from './cookie-banner/cookie-banner.component';
import { MapComponent } from './map/map.component';
import { EntrySmallComponent } from './entry-small/entry-small.component';
import { EntryLargeComponent } from './entry-large/entry-large.component';
import { MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material/snack-bar';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    HeaderComponent,
    MerchantRegisterComponent,
    InputComponent,
    ImagePickerComponent,
    CookieBannerComponent,
    MapComponent,
    EntrySmallComponent,
    EntryLargeComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBDRo19tiiI6-sm2yzbQcm4oUGV-cWHllI'
    }),
    FormsModule,
    CKEditorModule,
    ReactiveFormsModule,
    MatStepperModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [
    {provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 5000}}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
