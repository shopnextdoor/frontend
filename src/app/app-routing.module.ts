import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { MerchantRegisterComponent } from './merchant-register/merchant-register.component';
import { MapComponent } from './map/map.component';


const routes: Routes = [
  {path:'', component: LandingComponent},
  {path:'merchant/register', component: MerchantRegisterComponent},
  {path:'customer', component: MapComponent},
  {path:'customer/entry/:id', component: MapComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
