import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-image-picker',
  templateUrl: './image-picker.component.html',
  styleUrls: ['./image-picker.component.scss']
})
export class ImagePickerComponent implements OnInit {
  @Input() label: string;
  @Output() onChange = new EventEmitter<any>();
  data: any;
  constructor() { }

  ngOnInit(): void {
  }
  setImage(files: any){
    if (files.length === 0){
    return;
    }

  const mimeType = files[0].type;
  if (mimeType.match(/image\/*/) == null) {
    return;
  }
  const reader = new FileReader();
  reader.readAsDataURL(files[0]); 
  reader.onload = (_event) => { 
    this.data = reader.result; 
    this.compressImage(this.data, 1000).then((v) =>{
      this.data = v;
      this.onChange.emit(v);
    });
   }
  }
  compressImage(src, limit) {
    return new Promise((res, rej) => {
      const img = new Image();
      img.src = src;
      img.onload = () => {
        const elem = document.createElement('canvas');
        let newX,newY;
        if(img.width>img.height){
          newX = Math.min(limit,img.width);
          newY = newX * img.height/img.width;
        } else {
          newY = Math.min(limit,img.height);
          newX = newY * img.width/img.height;
        }
        elem.width = newX;
        elem.height = newY;
        const ctx = elem.getContext('2d');
        ctx.drawImage(img, 0, 0, newX, newY);
        const data = ctx.canvas.toDataURL("image/jpeg",0.75);
        res(data);
      }
      img.onerror = error => rej(error);
    });
  }
}
