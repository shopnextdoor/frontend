import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  static API_URL = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getAllEntries(){
    return this.http.get<any>(ApiService.API_URL+'company/list');
  }
  addEntry(entry: any){
    return this.http.post<any>(ApiService.API_URL+'company', JSON.stringify(entry));
  }
}
