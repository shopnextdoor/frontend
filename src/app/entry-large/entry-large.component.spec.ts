import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntryLargeComponent } from './entry-large.component';

describe('EntryLargeComponent', () => {
  let component: EntryLargeComponent;
  let fixture: ComponentFixture<EntryLargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntryLargeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntryLargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
