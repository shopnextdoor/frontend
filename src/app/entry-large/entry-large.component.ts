import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-entry-large',
  templateUrl: './entry-large.component.html',
  styleUrls: ['./entry-large.component.scss']
})
export class EntryLargeComponent implements OnInit {
  @Input() entry: any;
  constructor() { }

  ngOnInit(): void {
  }

}
