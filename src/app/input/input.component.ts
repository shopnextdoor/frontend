import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => InputComponent)
    }
  ]
})
export class InputComponent implements OnInit, ControlValueAccessor {
  @Input() label: string;
  @Input() formControlName: string;
  @Input() hasHelp = true;
  value: string;
  help = false;
  constructor() { }

  ngOnInit(): void {
  }

  
  writeValue(value: any) {
    if (value) {
      this.value = value;
    }

  }

  propagateChange(value: string) {
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {}

}
