import { Component, OnInit, ViewChild } from '@angular/core';
import { HeaderComponent } from '../header/header.component';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { MatStepper } from '@angular/material/stepper';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../api.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-merchant-register',
  templateUrl: './merchant-register.component.html',
  styleUrls: ['./merchant-register.component.scss']
})
export class MerchantRegisterComponent implements OnInit {
  Editor = ClassicEditor;
  @ViewChild('stepper') stepper: MatStepper;
  mainGroup: FormGroup;
  descriptionGroup: FormGroup;
  imageGroup: FormGroup;
  contactGroup: FormGroup;
  images:any = {};
  industries = [
    {key:"baecker", value: "Bäcker"},
    {key:"friseur", value: "Friseur"}
  ]
  constructor(private _formBuilder: FormBuilder, 
    private api: ApiService,
    private router: Router,
    private snackBar: MatSnackBar) {
      HeaderComponent.instance.title = 'Händlerregistrierung';
      this.mainGroup = this._formBuilder.group({
        companyName: ['Beispiel Firma', [Validators.required]],
        ownerName: ['Max Mustermann', [Validators.required]],
        industry: ['', [Validators.required]],
        zip: ['030', [Validators.required]],
        city: ['Berlin', [Validators.required]],
        street: ['Karl-Marx-Straße 30', [Validators.required]],
      });
      this.descriptionGroup = this._formBuilder.group({
        description: ['Lorem ipsum...', [Validators.required]],
      })
      this.contactGroup = this._formBuilder.group({
        contactTimes: ['Mo - Sa 08:00 - 18:00', [Validators.required]],
        phone: ['', []],
        email: ['meinunternehmen@web.de', [Validators.email]],
        facebook: ['', []],
        web: ['', []],
      });
      this.imageGroup = this._formBuilder.group({
      });
      /*
      setTimeout(() => {   
      this.next();
      this.next();
      this.next();
      });
      */
   }

  ngOnInit(): void {
  }
  next(){
    this.stepper.next();
  }

  save(){
    const json = this.mainGroup.getRawValue();
    json.industry = json.industry.key;
    for(const key of Object.keys(this.contactGroup.getRawValue())){
      json[key] = this.contactGroup.getRawValue()[key];
    }
    for(const key of Object.keys(this.descriptionGroup.getRawValue())){
      json[key] = this.descriptionGroup.getRawValue()[key];
    }
    json.images = [];
    for(let key of Object.keys(this.images)){
      let splitted = key.split('_');
      let position:number|string = 0;
      if(splitted.length > 1){
        key = splitted[0];
        position = splitted[1];
      }
      const image={
        type: key,
        position: position,
        base64: this.images[key]
      }
      json.images.push(image);
    }
    console.log(json,JSON.stringify(json).length / 1024);
    console.log(JSON.stringify(json));

    this.api.addEntry(json).subscribe(()=>{
      this.snackBar.open('Ihr Eintrag ist veröffentlicht. Vielen Dank!');
      this.router.navigate(['']);
    }, (error) => {
      this.snackBar.open('Fehler beim Veröffentlichen. Bitte erneut versuchen!');
      // @TODO are we going to have errors?
    });
  }
  display(industry: any){
    return industry.value;
  }
}
