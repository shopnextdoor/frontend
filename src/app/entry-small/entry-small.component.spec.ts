import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntrySmallComponent } from './entry-small.component';

describe('EntrySmallComponent', () => {
  let component: EntrySmallComponent;
  let fixture: ComponentFixture<EntrySmallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntrySmallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntrySmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
