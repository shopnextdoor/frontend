import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-entry-small',
  templateUrl: './entry-small.component.html',
  styleUrls: ['./entry-small.component.scss']
})
export class EntrySmallComponent implements OnInit {
  @Input() entry: any;
  constructor() { }

  ngOnInit(): void {
  }

}
